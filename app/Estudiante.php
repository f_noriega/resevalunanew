<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table='estudiante';
	protected $fillable=['id','idusuario','idcarrera'];

	public function Usuario()
	{
		return $this->belongsTo('App\Usuario', 'idusuario', 'id');
	}

	public function Carrera()
	{
		return $this->belongsTo('App\Carrera', 'idcarrera', 'id');
	}
}


/* Cuando se vaya a ingresar en inscripcion la materia del usuario, hacer una comparacion con la tabla usuarioEstudiante (Modelo Estudiante) y las materias y guardar el id de la materia de la table Materias, de tal forma que si el estudiante cambia de carrera
se mantenga el nombre de la materia en ese lapso . NO RELACIONARLO CON LA TABLA estudiante, ya que si cambio de carrera se vera afectado en inscripcion tb.



	Igual en el caso del profesor!!, colocar el id directo del USUARIO del profesor (de la tabla usuario), no el id de la tabla profesor, xq si cambia de materias pierdo el dato*/ 