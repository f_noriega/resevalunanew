<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table='profesor';
	protected $fillable=['id','idusuario','idmateria'];

	public function Usuario()
	{
		return $this->belongsTo('App\Usuario', 'idusuario', 'id');
	}

	public function Materia()
	{
		return $this->belongsTo('App\Materia', 'idmateria', 'id');
	}
}
