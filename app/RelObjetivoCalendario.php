<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelObjetivoCalendario extends Model
{
    protected $table='relolobjetivocalendario';
	protected $fillable=['id','idcalendario','objetivo'];

	public function Calendario()
	{
		return $this->belongsTo('App\Calendario', 'idcalendario', 'id');
	}

	/*
		Esta tabla es la que te indica los objetivos que se evaluan en las fechas indicadas
	*/
}
