<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    protected $table='inscripcion';
	protected $fillable=['id','idestudiante','idmateria','idlapso','idcarrera','idstatus'];

	public function Estudiante()
	{
		return $this->belongsTo('App\Usuario', 'idestudiante', 'id');
	}
	public function Materia()
	{
		return $this->belongsTo('App\Materia', 'idmateria', 'id');
	}
	public function Lapso()
	{
		return $this->belongsTo('App\Lapso', 'idlapso', 'id');
	}
	public function Carrera()
	{
		return $this->belongsTo('App\Carrera', 'idcarrera', 'id');
	}
}

/* Cuando se vaya a ingresar en inscripcion la materia del usuario, hacer una comparacion con la tabla estudiante (Modelo Estudiante) y las materias y guardar el id de la materia de la table Materias, de tal forma que si el estudiante cambia de carrera
se mantenga el nombre de la materia en ese lapso . NO RELACIONARLO CON LA TABLA estudiante, ya que si cambio de carrera se vera afectado en inscripcion tb.

	Igual en el caso del profesor!!, colocar el id directo del USUARIO del profesor (de la tabla usuario), no el id de la tabla profesor, xq si cambia de materias pierdo el dato*/ 