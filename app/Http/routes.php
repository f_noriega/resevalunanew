<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Ruta de Inicio
Route::get('/', function () {
    return view('principal');
});

//Rutas de Lapso
Route::resource('lapso','LapsoController');

//Rutas de Carreras
Route::resource('carrera','CarreraController');

//Rutas de Materias
Route::resource('materia','MateriaController');

//Rutas de Pensum
Route::resource('pensum','PensumController');

//Rutas de Plan Evaluacion
Route::resource('planevaluacion','PlanEvaluacionController');
Route::get('planevaluacion/nuevo/{idl}/{idm}/{c}/{o}','PlanEvaluacionController@nuevo');
Route::get('planevaluacion/ver/{idl}/{idm}','PlanEvaluacionController@ver');

//Rutas de Centros
Route::resource('centros','CentrosController');

//Rutas de Unidades
Route::resource('unidades','UnidadesController');

//Rutas de Usuario
Route::resource('usuarios','UsuariosController');
Route::resource('estudiantes','EstudianteController');
Route::resource('profesores','ProfesorController');