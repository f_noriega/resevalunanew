<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Unidades;
use App\Carrera;
use DB;
class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidades=Unidades::all();
        $carreras=Carrera::all();
        return view('gestioncentro.usuario.estudiante.form', compact('unidades','carreras'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idtipo=$request->idtipo;
        $idunidadapoyo=$request->idunidadapoyo;
        $nombre=$request->nombre;
        $apellido=$request->apellido;
        $cedula=$request->cedula;
        $email=$request->correo;
        $password=$request->clave;
        $idcarrera=$request->idcarrera;
        //dd($request->all());
        
        $id = DB::table('usuario')->insertGetId([
                         'idtipo' => $idtipo, 'idunidadapoyo' => $idunidadapoyo,'nombre' => $nombre, 'apellido' => $apellido, 'cedula' => $cedula, 'email' => $email , 'password' => $password
                    ]);

        DB::table('carreraestudiante')->insert(['idusuario'=>$id,'idcarrera'=>$idcarrera]);

       

        return redirect('usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
