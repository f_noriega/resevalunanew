<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Usuario;
use App\Unidades;
use DB;
class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios=Usuario::all();
        $estudiantes=Usuario::where('idtipo','=',3)->get();
        $profesores=Usuario::where('idtipo','=',2)->get();
        $administradores=Usuario::where('idtipo','=',1)->get();

        return view('gestioncentro.usuario.index',compact('usuarios','estudiantes','profesores','administradores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidades=Unidades::all();
        return view('gestioncentro.usuario.form', compact('unidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idtipo=$request->idtipo;
        $idunidadapoyo=$request->idunidadapoyo;
        $nombre=$request->nombre;
        $apellido=$request->apellido;
        $cedula=$request->cedula;
        $email=$request->correo;
        $password=$request->clave;
        //dd($request->all());
        DB::table('usuario')->insert([
                         ['idtipo' => $idtipo, 'idunidadapoyo' => $idunidadapoyo,'nombre' => $nombre, 'apellido' => $apellido, 'cedula' => $cedula, 'email' => $email , 'password' => $password],
                    ]);

        return redirect('usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
