<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PlanEvaluacion;
use App\Carrera;
use App\Materia;
use App\Lapso;
use App\Evaluacion;
use DB;
class PlanEvaluacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planesevaluacion=PlanEvaluacion::groupBy('idmateria')->paginate(17);
        $lapsos=Lapso::all();
        $materias=Materia::all();
        $carreras=Carrera::all();
        return view('gestionlapso.planevaluacion.index',compact('planesevaluacion','materias','lapsos','carreras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /* Notas: Tomar en cuenta:
        Se generan los objetivos y criterios de cada materia y evaluacion
    */ 
    public function nuevo($idl,$idm,$c,$o){
        $idlapso=$idl;
        $idmateria=$idm;
        $criterio=$c;
        $objetivo=$o;
        //dd($idlapso);
        return view('gestionlapso.planevaluacion.ajax.nuevoplan',compact('idlapso','idmateria','criterio','objetivo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
       /* 
            Esto va en CalendarioController
       $idmateria=$request->idmateria;
        $idlapso=$request->idlapso;
        $planevaluacion=$request->obj;
        $criterio=$request->criterio;
        $nobjetivos=$request->nobjetivo;
        $fechas=$request->fecha;
        $num=0;
        foreach($planevaluacion as $evaluacion){
            $num++;
                $id = DB::table('planevaluacion')->insertGetId(
                          ['idmateria' => $idmateria, 'idlapso' => $idlapso,'fecha' =>$fechas[$num] ]
                        );
            foreach($evaluacion as $objetivo){
                   DB::table('evaluaciones')->insert([
                         ['idplanevaluacion' => $id, 'objetivo' => $objetivo],
                    ]);
            }
        }*/
        return view('gestionlapso.planevaluacion.ajax.nuevoplan',compact('idlapso','idmateria','criterio','objetivo'));
        //return redirect('planevaluacion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function ver($idl,$idm){
        $idlapso=$idl;
        $idmateria=$idm;
        $planevaluacion=PlanEvaluacion::where('idlapso','=',$idlapso)->where('idmateria','=',$idmateria)->get();
        $materia=Materia::where('id','=',$idmateria)->first();
        $evaluaciones=Evaluacion::all();
        //dd($idmateria);
        return view('gestionlapso.planevaluacion.ajax.verplan',compact('idlapso','idmateria','planevaluacion','evaluaciones','materia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
