<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanEvaluacion extends Model
{
    protected $table='planevaluacion';
	protected $fillable=['id','idmateria','idlapso'];

	public function Materia()
	{
		return $this->belongsTo('App\Materia', 'idmateria', 'id');
	}
	public function Lapso()
	{
		return $this->belongsTo('App\Lapso', 'idlapso', 'id');
	}
}
