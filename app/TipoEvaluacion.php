<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEvaluacion extends Model
{
    protected $table='tipoevaluacion';
	protected $fillable=['id','nombre'];
}
