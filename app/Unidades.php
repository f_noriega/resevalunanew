<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidades extends Model
{
    protected $table='unidadesapoyo';
	protected $fillable=['id','idcentrolocal','nombre'];

	public function Centro()
	{
		return $this->belongsTo('App\Centros', 'idcentrolocal', 'id');
	}
}
