<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table='usuario';
	protected $fillable=['id','idtipo','idunidadapoyo','nombre','apellido','cedula','email','password'];

	public function Tipo()
	{
		return $this->belongsTo('App\Tipo', 'idtipo', 'id');
	}
	public function Unidad()
	{
		return $this->belongsTo('App\Unidades', 'idunidadapoyo', 'id');
	}
}
