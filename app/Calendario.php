<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{
    protected $table='calendario';
	protected $fillable=['id','idplanevaluacion','idtipoevaluacion','fecha'];

	public function PlanEvaluacion()
	{
		return $this->belongsTo('App\PlanEvaluacion', 'idevaluacion', 'id');
	}

	public function TipoEvaluacion()
	{
		return $this->belongsTo('App\TipoEvaluacion', 'idtipoevaluacion', 'id');
	}
}
