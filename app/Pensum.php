<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pensum extends Model
{
    protected $table='pensum';
	protected $fillable=['id','idmateria','idcarrera'];

	public function Materia()
	{
		return $this->belongsTo('App\Materia', 'idmateria', 'id');
	}
	public function Carrera()
	{
		return $this->belongsTo('App\Carrera', 'idcarrera', 'id');
	}
}
