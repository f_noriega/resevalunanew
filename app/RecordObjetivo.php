<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordObjetivo extends Model
{
    protected $table='estudiante';
	protected $fillable=['id','idestudiante','idobjetivoplan','idstatus'];

	public function Estudiante()
	{
		return $this->belongsTo('App\Estudiante', 'idestudiante', 'id');
	}

	public function ObjetivosdelPlan()
	{
		return $this->belongsTo('App\ObjetivosdelPlan	', 'idtipoevaluacion', 'id');
	}
}

/* Cuando el profesor ingresa la nota del estudiante , es en esta tabla
	donde se especifica los objetivos aprobados o no del mismo en del plan
	de evaluacion.
	
	Esta es la que se calcula cuando se cierra el lapso
	 */

