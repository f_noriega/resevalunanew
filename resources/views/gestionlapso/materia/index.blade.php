@extends('principal')

@section('contenedor')

	<div class="col-md-5">
		<h4>Lista de Materias</h4>
		<table class="table table-hover">
			@foreach($materias as $materia)
				<tr>
				  <td class="info">{{$materia->nombre}}</td>
				  <td class="text-center">
						<a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach

		</table>
		{{ $materias->links() }}
	</div>
	<div class="col-md-4">
		@include('gestionlapso.materia.form')
	</div>
	
@endsection