@extends('principal')

@section('contenedor')

	<div class="col-md-5">
		<h4>Lista de Lapsos</h4>
		<table class="table table-hover">
			@foreach($lapsos as $lapso)
				<tr>
				  <td class="info">Lapso {{$lapso->nombre}}</td>
				  <td class="text-center">
						<a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach

		</table>
		{{ $lapsos->links() }}
	</div>
	<div class="col-md-4">
		@include('gestionlapso.lapso.form')
	</div>
	
@endsection