@extends('principal')

@section('contenedor')

	<div class="col-md-5">
		<h4>Lista de Carreras</h4>
		<table class="table table-hover">
			@foreach($carreras as $carrera)
				<tr>
				  <td class="info">{{$carrera->nombre}}</td>
				  <td class="text-center">
						<a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach

		</table>
		{{ $carreras->links() }}
	</div>
	<div class="col-md-4">
		@include('gestionlapso.carrera.form')
	</div>
	
@endsection