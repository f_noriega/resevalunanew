@if(isset($edit))
	{!! Form::model($edit,['route'=>['carrera.update',$edit], 'method' => 'PUT'])!!}
@else
	{!!Form::open(['route' => 'carrera.store'],[ 'method'=>'POST'])!!}
@endif
		<div class="form-group">
			{!! Form::label('nombre','Identificador de carrera:') !!}
		</div>
		<div class="form-group">
			{!! Form::text('nombre',null, ['class' => 'form-control' , 'placeholder' => 'Nombre de Carrera']) !!}
		</div>
		@if(isset($edit))
			<button type="submit" class="btn btn-primary">Modificar</button>
		@else
			<button type="submit" class="btn btn-primary">Guardar</button>
		@endif	
	</div>
	{!! Form::close() !!}