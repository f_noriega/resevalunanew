<h4>Crear Nuevo Pensum</h4>
@if(isset($edit))
	{!! Form::model($edit,['route'=>['pensum.update',$edit], 'method' => 'PUT'])!!}
@else
	{!!Form::open(['route' => 'pensum.store'],[ 'method'=>'POST'])!!}
@endif
		
		<div class="form-group">
			{!! Form::label('carrera','Carrera:') !!}			
			<select id="carrera" class="form-control">
				<option value="">Seleccione Carrera</option>
				@foreach($carreras as $carrera)
				<option value="">{{$carrera->nombre}}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
		{!! Form::label('materia','Materia:') !!}
			<select id="carrera" class="form-control">
				<option value="">Seleccione Materia</option>
				@foreach($materias as $materia)
				<option value="">{{$materia->nombre}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
		{!! Form::label('uc','UC:') !!}
			{!! Form::number('uc',null,['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('requisito','Requisito:') !!}
			{!! Form::text('requisito',null,['class'=>'form-control','placeholder'=>'código de materia / uc']) !!}
			
		</div>
		@if(isset($edit))
			<button type="submit" class="btn btn-primary">Modificar</button>
		@else
			<button type="submit" class="btn btn-primary">Guardar</button>
		@endif	
	</div>
	{!! Form::close() !!}