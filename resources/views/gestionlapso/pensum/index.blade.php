@extends('principal')

@section('contenedor')
	<h2 class="text-center">Pensum</h2>
	<div class="col-md-5">
		<h4>Lista de Carreras</h4>
		<table class="table table-hover">
			@foreach($pensums as $pensum)
				<tr>
				  <td class="info">{{$pensum->Carrera->nombre}}</td>
				  <td class="text-center">
						<a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach

		</table>
		{{ $pensums->links() }}
	</div>
	<div class="col-md-4">
		@include('gestionlapso.pensum.form')
	</div>
	
@endsection