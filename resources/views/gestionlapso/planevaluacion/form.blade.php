
@if(isset($edit))
	{!! Form::model($edit,['route'=>['planevaluacion.update',$edit], 'method' => 'PUT'])!!}
@else
	{!!Form::open(['route' => 'planevaluacion.store'],[ 'method'=>'POST'])!!}
@endif
		<div class="form-group">
			<select id="carrera" class="form-control">
				<option value="">Seleccione Lapso</option>
				@foreach($carreras as $carrera)
				<option value="">{{$carrera->nombre}}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<select id="carrera" class="form-control">
				<option value="">Seleccione Materia</option>
				@foreach($materias as $materia)
				<option value="">{{$materia->nombre}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
		{!! Form::label('criterio','Criterio:') !!}
			{!! Form::number('criterio','0',['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('nobjetivos','N° Objetivos:') !!}
			{!! Form::number('nobjetivos',null,['class'=>'form-control','placeholder'=>'0']) !!}
			
		</div>
		

		@if(isset($edit))
			<button type="submit" class="btn btn-primary">Modificar</button>
		@else
			<button type="submit" class="btn btn-primary">Guardar</button>
		@endif	
	
	{!! Form::close() !!}