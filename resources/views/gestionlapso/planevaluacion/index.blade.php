@extends('principal')

@section('contenedor')
	<h2 class="text-center">Plan Evaluacion</h2>
	<div class="col-md-offset-3 col-md-5">
		<div class="form-group">
			
		</div>
		
		<h4>Plan Evaluacion</h4>
			@if(isset($edit))
				{!! Form::model($edit,['route'=>['planevaluacion.update',$edit], 'method' => 'PUT'])!!}
			@else
				{!!Form::open(['route' => 'planevaluacion.store'],[ 'method'=>'POST'])!!}
			@endif
		
				<div class="form-group">
					<select id="lapso" class="form-control" name="lapso">
						<option disabled selected value>Seleccione Lapso</option>
						@foreach($lapsos as $lapso)
						<option value="{{$lapso->id}}">Lapso {{$lapso->nombre}}</option>
						@endforeach
					</select>
				</div>
				<!--Select Materia -->
				<div class="form-group">
					<select id="materia" class="form-control" name="materia">
						<option disabled selected value>Seleccione Materia</option>
						@foreach($materias as $materia)
						<option value="{{$materia->id}}">{{$materia->nombre}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
				{!! Form::label('criterio','Criterio:') !!}
					{!! Form::number('criterio','0',['class'=>'form-control']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('nobjetivos','N° Objetivos:') !!}
					{!! Form::number('nobjetivos',null,['class'=>'form-control','placeholder'=>'0','id'=>'nobjetivo']) !!}
					<button  id="generarobje" type="button" class="btn btn-success" style="margin-right: 5px;" onclick="crearObjetivo()">Generar</button>
					
				</div>

				
				<div id="camposobje" class="row">
					
                           
				</div>
				
				<div class="row" style="margin-top: 20px;">
					<button  id="verplan" type="button" class="btn btn-success" style="margin-right: 5px;" data-toggle="modal" data-target="#myModal">Ver</button>
					<button  type="submit"  class="btn btn-primary ">Crear Nuevo Plan de Evaluacion</button>
					<!--<button id="crearplan"  type="button"  class="btn btn-primary " data-toggle="modal" data-target="#myModal1">Crear Nuevo Plan de Evaluacion</button>-->
				</div>
			{!! Form::close() !!}
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">{{$materia->nombre}}</h4>
	      </div>
	      <div class="modal-body" id="modalverplan">
	        @include('gestionlapso.planevaluacion.form')
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Calendario de Evaluación</h4>
	      </div>
	      {!!Form::open(['route' => 'planevaluacion.store'],[ 'method'=>'POST'], ['id'=>'formS'])!!}
	      <div class="modal-body" id="modalplan">
	       		Procesando...
	       
	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" onclick="AgregarCampos();" class="btn btn-primary">Agregar Evaluacion</button>
	        <button type="submit" class="btn success">Guardar</button>
		   {!! Form::close() !!}
	      </div>
	    </div>
	  </div>
	</div>
	
@endsection