@extends('principal')

@section('contenedor')

	<div class="col-md-5">
		<h4>Lista de centros</h4>
		<table class="table table-hover">
			@foreach($centros as $centro)
				<tr>
				  <td class="info">Centro {{$centro->nombre}}</td>
				  <td class="text-center">
						<a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Ver Unidades de Apoyo"></i></a>
						<a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach

		</table>
		{{ $centros->links() }}
		<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#myModal">Nueva Unidad de Apoyo</button>
	</div>
	
	<div class="col-md-4">
		@include('gestioncentro.centro.form')
		 
	</div>
	
@endsection

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nueva Unidad de Apoyo</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>