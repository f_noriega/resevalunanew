@if(isset($edit))
	{!! Form::model($edit,['route'=>['centros.update',$edit], 'method' => 'PUT'])!!}
@else
	{!!Form::open(['route' => 'centros.store'],[ 'method'=>'POST'])!!}
@endif
		<div class="form-group">
			{!! Form::label('nombre','Estado:') !!}
		</div>
		<div class="form-group">
			{!! Form::text('nombre',null, ['class' => 'form-control' , 'placeholder' => 'Estado']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('nombre','Sede Principal:') !!}
		</div>
		<div class="form-group">
			{!! Form::text('unidadnombre',null, ['class' => 'form-control' , 'placeholder' => 'Ciudad']) !!}
		</div>

		@if(isset($edit))	
			<button type="submit" class="btn btn-primary">Modificar</button>
		@else
			<button type="submit" class="btn btn-primary">Guardar</button>
		@endif	
	</div>
	{!! Form::close() !!}

	
		