<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalEstudiante" id="crearE">Crear Estudiante</button>
<table class="table table-hover">
 				 <tr>
 				 	<td>Nombre:</td>
 				 	<td>C.I.:</td>
 				 	<td>Tipo:</td>
 				 	<td>Centro:</td>
 				 	<td>Email</td>
 				 </tr>
 				 @foreach($estudiantes as $usuario)
 				 	<tr>
 				 		<td>{{$usuario->nombre}} {{$usuario->apellido}}</td>
 				 		<td>{{$usuario->cedula}}</td>
 				 		<td>{{$usuario->Tipo->nombre}}</td>
 				 		<td><{{$usuario->Unidad->Centro->nombre}}/{{$usuario->Unidad->nombre}}</td>
 				 		<td>{{$usuario->email}}</td>
 				 	</tr>
 				 @endforeach
</table>

<!-- Modal -->
	<div class="modal fade" id="myModalEstudiante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Nuevo Usuario</h4>
	      </div>
	      <div class="modal-body" id="modalestudiante">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>