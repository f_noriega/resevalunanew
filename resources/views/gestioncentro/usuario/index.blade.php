@extends('principal')

@section('contenedor')

	<div>

  	<!-- Nav tabs -->
  	<ul class="nav nav-tabs" role="tablist">
    	<li role="presentation" class="active"><a href="#usuarios" aria-controls="usuarios" role="tab" data-toggle="tab">Usuarios</a></li>
    	<li role="presentation"><a href="#administradores" aria-controls="administradores" role="tab" data-toggle="tab">Administradores</a></li>
    	<li role="presentation"><a href="#profesores" aria-controls="profesores" role="tab" data-toggle="tab">Profesores</a></li>
    	<li role="presentation"><a href="#estudiantes" aria-controls="estudiantes" role="tab" data-toggle="tab">Estudiantes</a></li>
  	</ul>

  <!-- Tab panes -->	
  <div class="tab-content">
    	<div role="tabpanel" class="tab-pane active" id="usuarios">
    		@include('gestioncentro.usuario.usuario')
    	</div>
    	<div role="tabpanel" class="tab-pane" id="administradores">
    		@include('gestioncentro.usuario.administrador.administrador')
    	</div>
    	<div role="tabpanel" class="tab-pane" id="profesores">
    		@include('gestioncentro.usuario.profesor.profesor')
    	</div>
    	<div role="tabpanel" class="tab-pane" id="estudiantes">
    		@include('gestioncentro.usuario.estudiante.estudiante')
    	</div>
  </div>

</div>
	
@endsection