@if(isset($edit))
	{!! Form::model($edit,['route'=>['usuarios.update',$edit], 'method' => 'PUT'])!!}
@else
	{!!Form::open(['route' => 'usuarios.store'],[ 'method'=>'POST'])!!}
@endif
		<div class="form-group">
			{!! Form::label('nombre','Nombre:') !!}
			{!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese Nombre de Usuario']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('apellido','Apellido:') !!}
			{!! Form::text('apellido',null,['class'=>'form-control','placeholder'=>'Ingrese Apellido de Usuario']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('cedula','Cédula:') !!}
			{!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Ingrese Cédula de Usuario']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('correo','Correo:') !!}
			{!! Form::text('correo',null,['class'=>'form-control','placeholder'=>'Ingrese Correo de Usuario']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('clave','Clave:') !!}
			{!! Form::password('clave',null,['class'=>'form-control','placeholder'=>'Ingrese clave de Usuario']) !!}
		</div>

		<div class="form-group">
			<select id="centro" name="idunidadapoyo" class="form-control">
				<option value="">Seleccione Centro:</option>
				@foreach($unidades as $unidad)
				<option value="{{$unidad->id}}">{{$unidad->Centro->nombre}} / {{$unidad->nombre}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			{!! Form::hidden('idtipo',1,['class'=>'form-control']) !!}
		</div>
		@if(isset($edit))
			<button type="submit" class="btn btn-primary">Modificar</button>
		@else
			<button type="submit" class="btn btn-primary">Guardar</button>
		@endif	
	</div>
	{!! Form::close() !!}

